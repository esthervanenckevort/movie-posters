//
//  Result.swift
//  Movie Posters
//
//  Created by David van Enckevort on 21-07-18.
//  Copyright © 2018 David van Enckevort. All rights reserved.
//

import Foundation

/**
 * Search result container.
 */
struct SearchResult: Codable {
    /**
     * The array of movies.
     * Remark: only set if response == "True"
     */
    var movies: [Movie]?
    /**
     * The total number of results.
     * Remark: only set if response == "True"
     */
    var totalResults: String?
    /**
      * The status of the request. "True" if the request was successful, "False" otherwise
      */
    var response: String
    /**
      * The error message.
     * Remark: only set if response == "False"
      */
    var error: String?

    // Define the coding keys here, because they are inconsistent in use of uppercase.
    private enum CodingKeys: String, CodingKey {
        case movies = "Search"
        case totalResults = "totalResults"
        case response = "Response"
        case error = "Error"
    }
}
