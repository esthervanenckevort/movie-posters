//
//  OMDBAPI.swift
//  Movie Posters
//
//  Created by David van Enckevort on 21-07-18.
//  Copyright © 2018 David van Enckevort. All rights reserved.
//

import Foundation

/**
 * Implementation of the OMDb API.
 */
struct OMDBAPI {
    #error("Please, fill in your API key.")
    /**
     * The API Key to use.
     * - Remark: Request your own key at [https://www.omdbapi.com/apikey.aspx](https://www.omdbapi.com/apikey.aspx)
     */
    private static let key = ""
    /**
     * The URL for the RESTful Endpoint.
     */
    private static let url = URL(string: "https://www.omdbapi.com/")!
    /**
     * The query parameter for the API key.
     */
    private static let apikey = URLQueryItem(name: "apikey", value: OMDBAPI.key)
    /**
     * Page size of the search API.
     */
    private static let pageSize = 100
    /**
     * The fully configured JSONDecode instance used to decode the result JSON.
     */
    private let decoder: JSONDecoder
    /**
     * The URLSession used to query the API.
     */
    private let session: URLSession

    /**
     * Constructor of an OMDb API client
     * - Parameter session: The URLSession to use for network calls (default URLSession.shared)
     */
    init(session: URLSession = URLSession.shared) {
        self.session = session
        self.decoder = JSONDecoder()
    }

    /**
     * Helper function to create the search URL.
     * - Parameter text: The text to search for
     * - Parameter page: The page of the search results to request
     */
    private func makeSearchURL(for text: String, page: Int) -> URLRequest? {
        guard var components = URLComponents(url: OMDBAPI.url, resolvingAgainstBaseURL: false) else { return nil }
        var queryItems = [OMDBAPI.apikey]
        queryItems.append(URLQueryItem(name: "s", value: text))
        queryItems.append(URLQueryItem(name: "page", value: String(page)))
        queryItems.append(URLQueryItem(name: "type", value: "movie"))
        components.queryItems = queryItems
        guard let url = components.url else { return nil }
        var request = URLRequest(url: url)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("no-cache", forHTTPHeaderField: "Cache-Control")
        request.httpMethod = "GET"

        return request
    }

    /**
     * Helper function to print errors.
     * - Parameter message: The error message.
     * - Parameter response: The URLResponse object (default nil)
     * - Paramter error: The Error object (default nil)
     */
    private func printError(_ message: String, response: URLResponse? = nil, error: Error? = nil) {
        print(message)
        if let response = response {
            print("HTTP Response: \(response.description)")
        }
        if let error = error {
            print("Error: \(error.localizedDescription)")
        }
    }

    /**
     * Load a poster image for the given movie.
     * - Parameter movie: The movie for which to load the image.
     * - Parameter completion: The completion handler to process the resulting image data
     *
     * - Remark: It is the caller's responsibility to make sure that any UI code is running on the main thread.
    */
    func loadImage(for movie: Movie, completion: @escaping (Data?) -> (Void)) {
        guard movie.posterURL != "N/A", let url = URL(string: movie.posterURL) else {
            completion(nil)
            return
        }
        session.dataTask(with: url) {
            (data, response, error) in
            // Check that we received data and that the reponse code was a 200 OK code
            guard let data = data, let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200 else {
                self.printError("Failed to load poster from \(url).", response: response, error: error)
                completion(nil)
                return
            }
            completion(data)
        }.resume()
    }

    /**
     * Search OMDb for poster titles matching the given text.
     * - Parameter text: The text to search for.
     * - Parameter page: The page of results to fetch, defaults to the first page.
     * - Parameter completion: A completion handler that is called after each page of results has been loaded.
     *
     * - Remark: The completion handler may be called multiple times.
     * - Remark: It is the caller's responsibility to make sure that any UI code is running on the main thread.
     */
    func search(for text: String, completion handler: @escaping ([Movie]?, UUID) -> (Bool)) -> UUID {
        let taskId = UUID()
        performSearch(for: text, page: 1, task: taskId, completion: handler)
        return taskId
    }

    /**
     * Search OMDb for poster titles matching the given text.
     * - Parameter text: The text to search for.
     * - Parameter page: The page of results to fetch, defaults to the first page.
     * - Parameter task: The task associated with this search request
     * - Parameter completion: A completion handler that is called after each page of results has been loaded.
     *
     * - Remark: The completion handler may be called multiple times.
     * - Remark: It is the caller's responsibility to make sure that any UI code is running on the main thread.
     */
    private func performSearch(for text: String, page: Int, task: UUID, completion handler: @escaping ([Movie]?, UUID) -> (Bool)) {
        guard let url = makeSearchURL(for: text, page: page) else {
            _ = handler(nil, task)
            return
        }
        session.dataTask(with: url) { (data, response, error) in
            guard let json = data, let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200 else {
                self.printError("Failed to search OMDb with \(url).", response: response, error: error)
                return
            }
            do {
                let result = try self.decoder.decode(SearchResult.self, from: json)
                let shouldContinue = handler(result.movies, task)

                // Calculate if there are more pages to fetch.
                if shouldContinue == true,
                    let totalResults = result.totalResults,
                    let count = Int(totalResults),
                    OMDBAPI.pageSize * (page - 1) + (result.movies?.count ?? 0) < count {

                    self.performSearch(for: text, page: page + 1, task: task, completion: handler)
                }
            } catch {
                self.printError("Failed to convert JSON data", error: error)
                _ = handler(nil, task)
            }
        }.resume()
    }
}
