//
//  Poster.swift
//  Movie Posters
//
//  Created by David van Enckevort on 21-07-18.
//  Copyright © 2018 David van Enckevort. All rights reserved.
//

import Foundation

/**
 * Structure for the movie data.
 */
struct Movie: Codable, Hashable {
    /**
     * Title of the movie
     */
    var title: String
    /**
     * Year the movie was released.
     */
    var year: String
    /**
     * ID of the movie in the [IMDb](https://www.imdb.com/) database.
     */
    var imdbID: String
    /**
     * Type of work.
     * Possible values: movie, game, series, episode
     */
    var type: String
    /**
     * URL for the poster.
     * Remark: This will be set to N/A when no poster image is available.
     */
    var posterURL: String

    // Define the coding keys here, because they are inconsistent in use of uppercase.
    private enum CodingKeys: String, CodingKey {
        case title = "Title"
        case year = "Year"
        case imdbID = "imdbID"
        case type = "Type"
        case posterURL = "Poster"
    }
}
