//
//  UIImage+Extensions.swift
//  Movie Posters
//
//  Created by David van Enckevort on 21-07-18.
//  Copyright © 2018 David van Enckevort. All rights reserved.
//

import UIKit

extension UIImage {

    /**
     * Create a resized version of this image.
     * - Parameter size: The new size of the image.
     * - Parameter preservingAspectRatio: Indicate if the aspect ratio of the image should be preserved. Default is true.
     * Remark: The size of the resulting image may differ from the given size if preservingAspectRatio is set to true.
     */
    func resized(to size: CGSize, preservingAspectRatio: Bool = true) -> UIImage {
        var newSize = size
        if preservingAspectRatio {
            let widthRatio = self.size.width / size.width
            let heightRatio = self.size.height / size.height
            let scale = max(widthRatio, heightRatio)
            newSize = self.size.scaled(by: scale)
        }
        return UIGraphicsImageRenderer(size: newSize).image { (context) in
            self.draw(in: CGRect(origin: .zero, size: newSize))
        }
    }
}
