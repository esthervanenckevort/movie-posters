//
//  CGSize+Extensions.swift
//  Movie Posters
//
//  Created by David van Enckevort on 21-07-18.
//  Copyright © 2018 David van Enckevort. All rights reserved.
//

import UIKit

extension CGSize {
    /**
     * Create a CGSize of this size scaled by the given factor.
     * - Parameter ratio: the ratio by which to scale.
     */
    func scaled(by ratio: CGFloat) -> CGSize {
        return CGSize(width: self.width / ratio, height: self.height / ratio)
    }
}
