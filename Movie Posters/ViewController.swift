//
//  ViewController.swift
//  Movie Posters
//
//  Created by David van Enckevort on 21-07-18.
//  Copyright © 2018 David van Enckevort. All rights reserved.
//

import UIKit

/**
 * The ViewController for the main screen.
 */
class ViewController: UIViewController {
    /**
     * The size constraints for the thumbnail image.
     */
    private static let imageSize = CGSize(width: 150, height: 150)
    /**
     * The tableview to display the movies with the posters.
     */
    @IBOutlet weak var tableView: UITableView!
    /**
     * The searchbar to search for movies.
     */
    @IBOutlet weak var searchBar: UISearchBar!
    /**
     * The model data backing the table.
     */
    private var movies = [Movie]()
    /**
     * The API implementation to fetch the movie data.
     */
    private let omdbAPI = OMDBAPI()
    /**
     * Cache for the thumbnail images of the posters.
     */
    private let thumbnailCache = NSCache<NSString, UIImage>()
    /**
     * Placeholder image for the poster while the actual image is loading.
     */
    private let placeholderPosterImageLoading = UIImage(named: "posterImageLoading")!
    /**
     * Placeholder image for the poster if no image is available.
     */
    private let placeholderNoPosterImage = UIImage(named: "noPosterImage")!
    /**
     * The ID of the search task used for cancellation.
     */
    private var searchTask: UUID?

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.prefetchDataSource = self
        tableView.rowHeight = ViewController.imageSize.height
        searchBar.delegate = self
        searchBar.enablesReturnKeyAutomatically = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Discard the cached thumbnails if memory is low.
        thumbnailCache.removeAllObjects()
    }

    /**
     * Update the cache.
     * - Parameters:
     *   - indexPath: the key to update.
     *   - image: the image to store in the cache.
     */
    private func updateCache(for movie: Movie, with image: UIImage) {
        self.thumbnailCache.setObject(image, forKey: movie.posterURL as NSString)
    }

    /**
     * Retrieve the cached image for the movie.
     * - Parameter movie: the movie for which the image is requested.
     * - Returns: The cached image or nil if no image was stored in the cache.
     */
    private func getCachedImage(for movie: Movie) -> UIImage? {
        return thumbnailCache.object(forKey: movie.posterURL as NSString)
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ViewController.imageSize.height
    }
}

extension ViewController: UITableViewDataSourcePrefetching {
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        for indexPath in indexPaths {
            let movie = movies[indexPath.row]
            loadImage(for: movie) { _ in }
        }
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movies.count
    }

    /**
     * Configure the cell with the given movie.
     * - Paramter cell: The cell to configure
     * - Parameter movie: The movie to display in the cell.
     */
    private func configure(cell: UITableViewCell, with movie: Movie) {
        cell.tag = movie.hashValue
        cell.textLabel?.text = movie.title
        cell.detailTextLabel?.text = movie.year
    }

    /**
     * Configure the cell with a new thumbnail image.
     * - Parameter cell: The cell to configure
     * - Parameter thumbnail: The thumbnail image to display in the cell.
     */
    private func configure(cell: UITableViewCell, with thumbnail: UIImage) {
        cell.imageView?.image = thumbnail
        cell.imageView?.sizeToFit()
        cell.setNeedsLayout()
    }

    /**
     * Load an image from the network.
     * - Parameters:
     *   - movie: The movie for which to fetch the image.
     *   - handler: The completion handler called when the image is loaded.
     * - Remark: It is the responsibility of the caller to make sure code is handled on the right thread.
     */
    private func loadImage(for movie: Movie, completion handler: @escaping (UIImage?) -> (Void)) {
        guard movie.posterURL != "N/A" else {
            handler(placeholderNoPosterImage)
            return
        }
        omdbAPI.loadImage(for: movie) { [weak self] in
            guard let self = self, let data = $0, let image = UIImage(data: data) else {
                handler(nil)
                return
            }
            let thumbnail = image.resized(to: ViewController.imageSize)
            self.updateCache(for: movie, with: thumbnail)
            handler(thumbnail)
        }
    }

    /**
     * Configure the cell's imageView with the right image.
     * - Parameters:
     *   - cell: The UITableViewCell to configure
     *   - movie: The movie that is displayed in the cell.
     */
    private func configureImage(for cell: UITableViewCell, with movie: Movie) {
        if let image = getCachedImage(for: movie) {
            configure(cell: cell, with: image)
        } else {
            configure(cell: cell, with: placeholderPosterImageLoading)
            loadImage(for: movie) { [weak self] thumbnail in
                DispatchQueue.main.async { [weak self] in
                    guard let self = self, cell.tag == movie.hashValue else {
                        return
                    }
                    if let thumbnail = thumbnail {
                        self.configure(cell: cell, with: thumbnail)
                    } else {
                        self.configure(cell: cell, with: self.placeholderNoPosterImage)
                    }
                }
            }
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let movie = movies[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "Poster", for: indexPath)
        configure(cell: cell, with: movie)
        configureImage(for: cell, with: movie)
        return cell
    }
}

extension ViewController: UISearchBarDelegate {

    /**
     * Clear the results of the last search in the model.
     */
    private func clearResults() {
        tableView.beginUpdates()
        let indexPaths = movies.enumerated().map { return IndexPath(row: $0.offset, section: 0) }
        tableView.deleteRows(at: indexPaths, with: .none)
        movies.removeAll()
        tableView.endUpdates()
    }

    /**
     * Append the results to the model.
     */
    private func appendResults(_ newMovies: [Movie]) {
        tableView.beginUpdates()
        let count = movies.count
        movies.append(contentsOf: newMovies)
        let indexPaths = newMovies.enumerated().map { return IndexPath(row: $0.offset + count, section: 0) }
        tableView.insertRows(at: indexPaths, with: .none)
        tableView.endUpdates()
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        guard let text = searchBar.text else {
            clearResults()
            return
        }
        clearResults()
        searchTask = omdbAPI.search(for: text) {
            [weak self] movies, task in
            guard let self = self else { return false }
            DispatchQueue.main.async { [weak self] in
                guard let self = self, let movies = movies, self.searchTask == task else {
                    return
                }
                self.appendResults(movies)
            }
            return self.searchTask == task
        }
    }
}
